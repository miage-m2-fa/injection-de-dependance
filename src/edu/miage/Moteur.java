package edu.miage;

public class Moteur {

	private String type;

	public Moteur() {
	}

	public Moteur(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean demarrer() {
		System.out.println("Le moteur " + type + " a bien d�marr� :)");
		return true;
	}

}
