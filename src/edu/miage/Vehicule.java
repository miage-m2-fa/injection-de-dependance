package edu.miage;

public class Vehicule {

	private Moteur moteur;

	public Vehicule() {
	}

	public Moteur getMoteur() {
		return moteur;
	}

	public void setMoteur(Moteur moteur) {
		this.moteur = moteur;
	}

	public boolean demarrer() {
		return this.moteur.demarrer();
	}

}
